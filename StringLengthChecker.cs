﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LV6_6
{
    class StringLengthChecker : StringChecker
    {
        private int length;
        public StringLengthChecker()
        {
            this.length = 6;
        }
        protected override bool PerformCheck(string stringToCheck)
        {
            return (stringToCheck.Length >= this.length);
        }
    }
}
