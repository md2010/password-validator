﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LV6_6
{
    class PasswordValidator
    {
        private StringChecker first;
        private StringChecker lastAdded;
        public PasswordValidator(StringChecker first)
        {
            this.first = first;
            this.lastAdded = first;
        }        
        public bool ValidatePassword(string passwordToCheck)
        {
            bool result = false;
            if (this.first != null)
                result = this.first.Check(passwordToCheck);
            return result;            
        }
        public void SetNextInChain(StringChecker stringChecker)
        {
            this.lastAdded.SetNext(stringChecker);
            this.lastAdded = stringChecker;
        }

    }
}
