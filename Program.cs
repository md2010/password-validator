﻿using System;

namespace LV6_6
{
    class Program
    {
        static void Main(string[] args)
        {
            TestExercise6();
            TestExercise7();

        }
        static void TestExercise6()
        {
            StringChecker stringChecker = new StringLengthChecker();
            StringDigitChecker digitChecker = new StringDigitChecker();
            StringUpperCaseChecker upperCaseChecker = new StringUpperCaseChecker();
            StringLowerCaseChecker lowerCaseChecker = new StringLowerCaseChecker();
           
            stringChecker.SetNext(digitChecker);
            digitChecker.SetNext(upperCaseChecker);
            upperCaseChecker.SetNext(lowerCaseChecker);

            string somethingRandom = "hjdhjdZZ56hJ";
            string name = "Angelina";

            Console.WriteLine(stringChecker.Check(somethingRandom));
            Console.WriteLine(stringChecker.Check(name));
        }

        static void TestExercise7()
        {
            StringLengthChecker lengthChecker = new StringLengthChecker();
            StringDigitChecker digitChecker = new StringDigitChecker();
            StringUpperCaseChecker upperCaseChecker = new StringUpperCaseChecker();
            StringLowerCaseChecker lowerCaseChecker = new StringLowerCaseChecker();

            PasswordValidator passwordValidator = new PasswordValidator(lengthChecker);
            passwordValidator.SetNextInChain(digitChecker);
            passwordValidator.SetNextInChain(upperCaseChecker);
            passwordValidator.SetNextInChain(lowerCaseChecker);

            string password1 = "bd7yra28";
            Console.WriteLine(passwordValidator.ValidatePassword(password1));

            string password2 = "bD7Yra28";
            Console.WriteLine(passwordValidator.ValidatePassword(password2));
        }
    }
}
