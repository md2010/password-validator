﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;

namespace LV6_6
{
    class StringLowerCaseChecker : StringChecker
    {
        protected override bool PerformCheck(string stringToCheck)
        {           
            return (stringToCheck.Any(char.IsLower));
        }
    }
}
